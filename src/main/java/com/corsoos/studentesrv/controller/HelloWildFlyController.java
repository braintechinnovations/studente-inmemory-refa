package com.corsoos.studentesrv.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.corsoos.studentesrv.service.StudentService;
import com.corsoos.studentesrv.model.Course;

@RestController
public class HelloWildFlyController {


    @RequestMapping("test")
    public String sayHello(){
        return ("Hello, Fkn Wildfly");
    }
    
    @Autowired
	private StudentService studentService;

	@GetMapping("/students/{studentId}/courses")
	public List<Course> retrieveCoursesForStudent(@PathVariable String studentId) {
		return studentService.retrieveCourses(studentId);
	}
}